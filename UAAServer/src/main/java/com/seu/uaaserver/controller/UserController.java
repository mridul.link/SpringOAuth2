package com.seu.uaaserver.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(value = "/rest/user")
public class UserController {

    @GetMapping(value = "/info")
    public Principal getUser(Principal principal){
        return principal;
    }
}
