package com.seu.uaaserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter{

    @Autowired
    AuthenticationManager authenticationManager;

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("clientId")
                .secret("secret")
                .authorizedGrantTypes("authorization_code","password","refresh_token")
                .scopes("user_info","read","write")
                .accessTokenValiditySeconds(120)
                .refreshTokenValiditySeconds(120)
                .autoApprove(true);
        clients.withClientDetails(new ClientDetailsService() {
            @Override
            public ClientDetails loadClientByClientId(String s) throws ClientRegistrationException {
                return new ClientDetails() {
                    @Override
                    public String getClientId() {
                        return null;
                    }

                    @Override
                    public Set<String> getResourceIds() {
                        return null;
                    }

                    @Override
                    public boolean isSecretRequired() {
                        return false;
                    }

                    @Override
                    public String getClientSecret() {
                        return null;
                    }

                    @Override
                    public boolean isScoped() {
                        return false;
                    }

                    @Override
                    public Set<String> getScope() {
                        return null;
                    }

                    @Override
                    public Set<String> getAuthorizedGrantTypes() {
                        return null;
                    }

                    @Override
                    public Set<String> getRegisteredRedirectUri() {
                        return null;
                    }

                    @Override
                    public Collection<GrantedAuthority> getAuthorities() {
                        return null;
                    }

                    @Override
                    public Integer getAccessTokenValiditySeconds() {
                        return null;
                    }

                    @Override
                    public Integer getRefreshTokenValiditySeconds() {
                        return null;
                    }

                    @Override
                    public boolean isAutoApprove(String s) {
                        return false;
                    }

                    @Override
                    public Map<String, Object> getAdditionalInformation() {
                        return null;
                    }
                };
            }
        });
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager);
    }
}
