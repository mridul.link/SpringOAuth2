package com.seu.facultyservice.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class UserController {

    @PreAuthorize(value = "hasAnyRole('USER')")
    @GetMapping(value = "/v1/info_user")
    public String getUserInfoUser(){
        return "Mridul";
    }

    @PreAuthorize(value = "hasAnyRole('ADMIN')")
    @GetMapping(value = "/v1/info_admin")
    public String getUserInfoAdmin(){
        return "KMH";
    }
}
